package br.up.edu.oapp05;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mostrar(View v){

        //Pega as caixas de texto
        EditText cxNumero = (EditText) findViewById(R.id.txtNumero);
        EditText cxMes = (EditText) findViewById(R.id.txtMes);

        //Ler o texto da caixa
        String txtNumero = cxNumero.getText().toString();

        //Converter para número
        int numero = Integer.parseInt(txtNumero);

        switch (numero){
            case 1:
                cxMes.setText("JANEIRO");
                break;
            case 2:
                cxMes.setText("FEVEREIRO");
                break;
            case 3:
                cxMes.setText("MARÇO");
                break;
            case 4:
                cxMes.setText("ABRIL");
                break;
            case 5:
                cxMes.setText("MAIO");
                break;
            case 6:
                cxMes.setText("JUNHO");
                break;
            case 7:
                cxMes.setText("JULHO");
                break;
            case 8:
                cxMes.setText("AGOSTO");
                break;
            case 9:
                cxMes.setText("SETEMBRO");
                break;
            case 10:
                cxMes.setText("OUTUBRO");
                break;
            case 11:
                cxMes.setText("NOVEMBRO");
                break;
            case 12:
                cxMes.setText("DEZEMBRO");
                break;
            default:
                cxMes.setText("INVÁLIDO");
                break;
        }
    }
}
